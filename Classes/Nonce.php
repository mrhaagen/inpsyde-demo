<?php namespace Demo\Classes;

use Spatie;
use DateTime;

Interface Nonce {    
    /**
     * Appends the Nonce hash to a given URL
     * @param string $url Url where to append the Nonce to
     * @return Spatie\Url\Url The Url with the Nonce appended
     */
    public function wp_nonce_url(Spatie\Url\Url $url): Spatie\Url\Url;
    
    /**
     * Generates fields with Nonce for form validation
     * @param string $referer If this is not null or empty a hidden field is created with the content of this parameter. The content should be the result of a wp_referer_field call.
     * @return string The HTML code with the fields or null if $echo is true.
     */
    public function wp_nonce_field(string $referer=""): string;
    
    /**
     * Checks whether this Nonce is valid or not.
     * @return bool True, if the Nonce is valid. False else.
     */
    public function wp_verify_nonce(): bool;
    
    public function get_expiration_date(): DateTime;
    public function get_name(): string;
    public function get_hash(): string;
    public function get_action(): string;
}