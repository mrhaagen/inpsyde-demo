<?php namespace Demo\Classes;

use Spatie;
use DateTime;
use DateInterval;

/* 
 * This implementation of a Nonce counts a value upwards each time a Nonce is
 * created. This way each Nonce is unique.
 */
Class CountingDateTimeNonce implements Nonce {
    private static $nextId = 0;
    private $hash;
    private $name;
    private $action;
    private $expirationDate;
    
    function __construct(string $action="", string $name="_wpnonce", DateInterval $lifeTime=null){
        $dateTime = new DateTime();
        $this->hash = hash("sha256", CountingDateTimeNonce::$nextId."".$dateTime->format("Y-m-d H:i:s"));
        CountingDateTimeNonce::$nextId++;
        $this->action = $action;
        $this->name = $name;
        if (is_null($lifeTime)){
            $this->expirationDate = $dateTime->add(new DateInterval("P1D"));
            assert(!is_null($this->expirationDate));
        }
        else{
            $this->expirationDate = $dateTime->add($lifeTime);
            assert(!is_null($this->expirationDate));
        }
    }
    
    public function get_action(): string {
        return $this->action;
    }

    public function get_expiration_date(): \DateTime {
        return $this->expirationDate;
    }

    public function get_hash(): string {
        return $this->hash;
    }

    public function get_name(): string {
        return $this->name;
    }

    public function wp_nonce_field(string $referer=""): string {
        $result = '<input type="hidden" id="save_cpt_wpnonce" value="'.$this->hash.'">';
        if (!empty($referer)){
            $result = $result.'<input type="hidden" name="_wp_http_referer" value="'.$referer.'">';
        }
        return $result;
    }

    public function wp_nonce_url(Spatie\Url\Url $url): Spatie\Url\Url {
        $result = $url;
        
        if (!empty($this->action)){
            $result = $result->withQueryParameter("action", $this->action);
        }
        
        $result = $result->withQueryParameter($this->name, $this->hash);
        
        return $result;
    }

    public function wp_verify_nonce(): bool {
        $currentDateTime = new DateTime();
        return $currentDateTime < $this->expirationDate;
    }

}