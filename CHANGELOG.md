# Changelog

## c0b43d8
Added README

## 2a9b9e8
Created API with interfaces

## 86eaa4d
Implemented class "Nonce"

## b3ad024
Implemented class "CountingDateTimeNonce"

## 48f61c7
Removed parameter "echo" from function "wp_nonce_field" of class "Nonce"

## 63be71d
Enabled autoloading of classes via composer and added Unit tests for function
"wp_verify_nonce" of class "CountingDateTimeNonce"

## 171c546
Fixed constructor definitions and fixed format string in function
"testWpVerifyNonce_NonceInvalid()" of Unit test "TestCountingDateTimeNonce"

## 4f5f17f
Fixed functions "wp_nonce_field" and "wp_nonce_url" of class 
"CountingDateTimeNonce" and added Unit tests for functions "wp_nonce_field",
"wp_nonce_url" and "wp_verify_nonce" of class "CountingDateTimeNonce"

## 3308cfe
Added Unit tests for functions "wp_nonce_url" and "wp_verify_nonce" of class
"CountingDateTimeNonce"

## 2011465
Added license file